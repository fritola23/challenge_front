import React, { Component } from 'react';
import './styles/style.css';

import Header from './Components/header';
import Introduction from './Components/introduction';
import ipad from './assets/ipad.png';
import book from './assets/book1.jpg';

export default class App extends Component {
  componentDidMount() {
    console.log('oi');
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <Header />

          <div className="content_introduction">
            <Introduction />
            <img src={ipad} alt="ipad" />
          </div>

          <div className="content">
            <h1>Books</h1>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
              vitae eros eget tellus <br /> tristique bibendum. Donec rutrum sed
              sem quis venenatis.
            </p>
            <div className="books">
              <img src={book} alt="book" />
              <img src={book} alt="book" />
              <img src={book} alt="book" />
              <img src={book} alt="book" />

              <img src={book} alt="book" />
              <img src={book} alt="book" />
              <img src={book} alt="book" />
              <img src={book} alt="book" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
